package pe.edu.torres.alex.prototipobroadcastreceiver;

import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.telephony.SmsManager;
import android.telephony.SmsMessage;
import android.widget.Toast;

public class SMSReceiver extends BroadcastReceiver {
    // TODO SMSMANAGER CLASS
    final SmsManager sms = SmsManager.getDefault();

    @Override
    public void onReceive(Context context, Intent intent) {

        if(intent.getAction().equals("android.provider.Telephony.SMS_RECEIVED")) {

            final Bundle bundle = intent.getExtras();
            try {

                if (bundle != null) {
                    // TODO PDU PROTOCOL DATA UNIT STANDAR PARA SMS MESSAGE
                    final Object[] pdusObj = (Object[]) bundle.get("pdus");

                    for (int i = 0; i < pdusObj.length; i++) {
                        // TODO CREACION SMSMESSAGE OBJECT DESDE PDU RECIBIDO
                        SmsMessage sms = SmsMessage.createFromPdu((byte[]) pdusObj[i]);
                        // TODO EXTRAER TELEFONO Y MENSAJE
                        String phoneNumber = sms.getDisplayOriginatingAddress();
                        String message = sms.getDisplayMessageBody();
                        String formattedText = String.format(context.getResources().getString(R.string.sms_message), phoneNumber, message);
                        // TODO MOSTRAR SMS
                        Toast.makeText(context, formattedText, Toast.LENGTH_LONG).show();

                        MainActivity inst = MainActivity.instance();
                        inst.updateList(formattedText);
                    }
                }

            } catch (Exception e) {
                e.printStackTrace();
            }
        }
    }
}
